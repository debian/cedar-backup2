cedar-backup2 (2.27.0-1) unstable; urgency=medium

  Cedar Backup v2 is unsupported upstream as of 11 Nov 2017.  Cedar Backup
  v2.27.0 will be the final release.  

  This version of Cedar Backup v2 prints a large warning banner indicating
  that it is unsupported.  To hide this banner, use the --unsupported flag.

  My goal is to maintain the cedar-backup2 and cedar-backup2-doc packages for
  Debian buster, removing the packages from unstable immediately following
  buster's release.  The Python 2 end-of-life date is in 2020, so there is a
  chance that Python 2 will be removed from buster.  If that happens, I will
  also remove the cedar-backup2 and cedar-backup2-doc packages at the same
  time.

  Migrating from Cedar Backup v2 to Cedar Backup v3 is straightforward. The
  functionality is equivalent, but names have changed.  So, the command-line
  tool is cback3 instead of cback, the config file is /etc/cback3.conf instead
  of /etc/cback.conf, the cron jobs are under /etc/cron.d/cedar-backup3, etc.
  The two versions of the software can coexist on a single system, which should
  ease your migration effort.

 -- Kenneth J. Pronovici <pronovic@debian.org>  Sat, 11 Nov 2017 17:25:09 +0000

